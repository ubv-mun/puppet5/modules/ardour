# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include ardour::install
class ardour::install {
  package { $ardour::package_name:
    ensure => $ardour::package_ensure,
  }
}
